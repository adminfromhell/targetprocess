package targetprocess

import (
	"os"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGlobalVars(t *testing.T) {
	Convey("Given default global vars", t, func() {
		Convey("Check TPAddress", func() {
			So(TPAddress, ShouldEqual, "")
		})
		Convey("Check SkipTLS", func() {
			So(SkipTLS, ShouldBeFalse)
		})
		Convey("Check HTTPTimeout", func() {
			So(HTTPTimeout, ShouldEqual, 1*time.Minute)
		})
	})
	Convey("Changing default global vars", t, func() {
		Convey("Change TPAddress", func() {
			TPAddress = "https://test.tpondemand.com"
			So(TPAddress, ShouldEqual, "https://test.tpondemand.com")
		})
		Convey("Change SkipTLS", func() {
			SkipTLS = true
			So(SkipTLS, ShouldEqual, true)
		})
		Convey("Change HTTPTimeout", func() {
			HTTPTimeout = 10 * time.Minute
			So(HTTPTimeout, ShouldEqual, 10*time.Minute)
		})
	})
	Convey("Test ENV vars", t, func() {
		os.Setenv("TP_API_URL", "https://adminfromhellt.tpondemand.com/")
		os.Setenv("TP_ACCESS_TOKEN", "SomeAccessToken")
		os.Setenv("TP_USERNAME", "admin")
		os.Setenv("TP_PASSWORD", "password")
		initVars()
		So(TPApiURL, ShouldEqual, "https://adminfromhellt.tpondemand.com/")
		So(TPAccessToken, ShouldEqual, "SomeAccessToken")
		So(TPUsername, ShouldEqual, "admin")
		So(TPPassword, ShouldEqual, "password")
	})
	Convey("Create new Client", t, func() {
		nc := NewClient("admin", "admin")
		exToken := "YWRtaW46YWRtaW4="
		So(nc.TPAddress, ShouldEqual, TPAddress)
		So(nc.authToken, ShouldEqual, exToken)
		So(nc.http, ShouldNotBeNil)
	})
}
