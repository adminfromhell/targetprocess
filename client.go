package targetprocess

import (
	b64 "encoding/base64"
	"fmt"
	"time"

	"github.com/go-resty/resty"
	"github.com/spf13/viper"
)

var (
	// TPAddress is the api URL for target process
	TPAddress = ""
	// SkipTLS allows skipping TLS cert verification
	SkipTLS = false
	// HTTPTimeout sets the HTTP timeout for requests
	HTTPTimeout = 1 * time.Minute
	// ENV Vars
	// TPApiURL is set from the env var TP_API_URL
	TPApiURL interface{}
	// TPAccessToken is set from env var TP_ACCESS_TOKEN
	TPAccessToken interface{}
	// TPUsername is set from env var TP_USERNAME
	TPUsername interface{}
	// TPPassword is set from env var TP_PASSWORD
	TPPassword interface{}
)

// Client holds all TP Client data
type Client struct {
	http      *resty.Client
	authToken string
	TPAddress string
}

func init() {
	initVars()
}

func initVars() {
	viper.SetEnvPrefix("tp")
	viper.BindEnv("api_url")
	viper.BindEnv("access_token")
	viper.BindEnv("username")
	viper.BindEnv("password")
	// Set vars if env vars are present
	// TP_API_URL
	TPApiURL = viper.Get("api_url")
	// TP_ACCESS_TOKEN
	TPAccessToken = viper.Get("access_token")
	// TP_USERNAME
	TPUsername = viper.Get("username")
	// TP_PASSWORD
	TPPassword = viper.Get("password")
}

// NewClient returns a new TP Client
func NewClient(username, password string) *Client {
	c := Client{
		http:      resty.New(),
		authToken: generateAuthToken(username, password),
		TPAddress: TPAddress,
	}
	return &c
}

func generateAuthToken(username, password string) string {
	toEnc := fmt.Sprintf("%s:%s", username, password)
	sEnc := b64.StdEncoding.EncodeToString([]byte(toEnc))
	return sEnc
}
